import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Scanner;

/**
 * This is the implimentation of main class
 * 
 * @author agannarapu
 *
 */
public class FileIo {
	public static void main(String[] args) throws Exception {
		int choice = -1;
		Scanner scn = new Scanner(System.in);

		// Create a file with .txt
		File file = new File("user.txt");

		// Create a ArrayList object creation
		ArrayList<User> u = new ArrayList<User>();
		ObjectOutputStream obj = null;
		ObjectInputStream obj1 = null;
		ListIterator li = null;

		// If the file is available ,then user list will be added into file
		if (file.isFile()) {
			obj1 = new ObjectInputStream(new FileInputStream(file));
			u = (ArrayList<User>) obj1.readObject();
			obj1.close();

		}
		do {

			System.out.println("1.Insert");
			System.out.println("2.Display");
			System.out.println("3.Search");
			System.out.println("4.Delete");
			System.out.println("5.Update");
			System.out.println("0.Exit");

			// Select the choice of above operations
			System.out.print("Enter your choice:");
			choice = scn.nextInt();
			switch (choice) {
			case 1:
				// This is a case of Insert Operation
				System.out.println("Enter how many users you want:");
				int n = scn.nextInt();
				for (int i = 0; i < n; i++) {
					System.out.println("Enter name:");//name
					String name = scn.next();

					System.out.println("Enter Profile");//profile
					String profile = scn.next();

					System.out.println("Enter hobby");//hobby
					String hobby = scn.next();

					System.out.println("Enter age");//age
					int age = scn.nextInt();

					System.out.println("Enter salary");//salary
					int sal = scn.nextInt();

					u.add(new User(name, profile, hobby, age, sal));

				}
				try {
					//Create the object of ObjectOutputStream used to write the Java objects
					obj = new ObjectOutputStream(new FileOutputStream(file));
					obj.writeObject(u);
					obj.close();
				} catch (FileNotFoundException e) {

					e.printStackTrace();
				} catch (IOException e) {

					e.printStackTrace();
				}
				break;

			case 2:
				// This is the implementation of display operation
				if (file.isFile()) {
					
					//Create a object of ObjectInputStream used to read the objects 
					obj1 = new ObjectInputStream(new FileInputStream(file));
					u = (ArrayList<User>) obj1.readObject();
					obj1.close();
					System.out.println("------------------------------------");
					li = u.listIterator();
					while (li.hasNext())
						System.out.println(li.next());

					System.out.println("------------------------------------");
				} else {
					System.out.println("File Not Exists");
				}
				break;
			case 3:
				// This is the Implimentation of Search operation

				if (file.isFile()) {
					
					//Create a object of ObjectInputStream used to read the objects
					obj1 = new ObjectInputStream(new FileInputStream(file));
					u = (ArrayList<User>) obj1.readObject();
					obj1.close();
					boolean found = false;
					System.out.println("Enter name to search:");
					String name = scn.next();
					System.out.println("=======================");
					li = u.listIterator();
					while (li.hasNext()) {
						User u1 = (User) li.next();
						if (u1.name.equals(name)) {
							System.out.println(u1);
							found = true;
						}
					}
					if (!found) {
						System.out.println("Record not found");
					}
				}
				break;

			case 4:
				// This is the implimentation of delete operation

				if (file.isFile()) {
					
					//Create a object of ObjectInputStream used to read the objects
					obj1 = new ObjectInputStream(new FileInputStream(file));
					u = (ArrayList<User>) obj1.readObject();
					obj1.close();
					boolean found = false;
					System.out.println("Enter name to delete:");
					String name = scn.next();
					System.out.println("=======================");
					li = u.listIterator();
					while (li.hasNext()) {
						User u1 = (User) li.next();
						if (u1.name.equals(name)) {
							li.remove();
							found = true;
						}
					}
					if (found) {
						obj = new ObjectOutputStream(new FileOutputStream(file));
						obj.writeObject(u);
						obj.close();
						System.out.println("Record Deleted Successfully");

					} else {
						System.out.println("Record not found");
					}
				}
				break;
			case 5:
				// This is the implimentation of update operation

				if (file.isFile()) {
					//Create a object of ObjectInputStream used to read the objects
					obj1 = new ObjectInputStream(new FileInputStream(file));
					u = (ArrayList<User>) obj1.readObject();
					obj1.close();
					boolean found = false;
					System.out.println("Enter name to update:");
					String name = scn.next();
					System.out.println("=======================");
					li = u.listIterator();
					while (li.hasNext()) {
						User u1 = (User) li.next();
						if (u1.name.equals(name)) {
							System.out.println("Enter the new name");//new name
							String name1 = scn.next();

							System.out.println("Enter the profile");//new profile
							String profile = scn.next();

							System.out.println("Enter the hobby");//new hobby
							String hobby = scn.next();

							System.out.println("Enter the age");//new age
							int age = scn.nextInt();

							System.out.println("Enter the salary");//new salary
							int salary = scn.nextInt();

							li.set(new User(name1, profile, hobby, age, salary));

							found = true;
						}
					}
					if (found) {
						//If name found, update the object
						obj = new ObjectOutputStream(new FileOutputStream(file));
						obj.writeObject(u);
						obj.close();
						//If Record successfully updated
						System.out.println("Record Updated Successfully");

					} else {
						System.out.println("Record not found");
					}
				}
				break;

			}
		} while (choice != 0);
	}

}
