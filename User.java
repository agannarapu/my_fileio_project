import java.io.Serializable;
/**
 * This is the implimentation of  User class
 * @author agannarapu
 *
 */

public class User implements Serializable{
  String name;
  String profile;
  String hobby;
  int age;
  int salary;
  
  /**
   * 
   * @param name
   * @param profile
   * @param hobby
   * @param age
   * @param salary
   */
  User(String name,String profile,String hobby,int age,int salary){
	  this.name=name;
	  this.profile=profile;
	  this.hobby=hobby;
	  this.age=age;
	  this.salary=salary;
  }
  /**
   * Overriding the default toString method of User so that User can be printed in
   * human readable format.
   */
  public String toString() {
//	  return name+" "+profile+" "+hobby+" "+age+" "+salary;
	  String stringToReturn = "";
	    stringToReturn += "name                            :                " + this.name + "\n";
		stringToReturn += "Profile                         :                " + this.age + "\n";
		stringToReturn += "Hobby                           :                " + this.hobby + "\n";
		stringToReturn += "Age                             :                " + this.age + "\n";
		stringToReturn += "Salary                          :                " + this.salary + "\n";
		return stringToReturn;
  }
}
